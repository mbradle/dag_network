#///////////////////////////////////////////////////////////////////////////////
#  Copyright (c) 2017 Clemson University.
# 
#  This file was originally written by Bradley S. Meyer.
# 
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this software; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#  USA
# 
#/////////////////////////////////////////////////////////////////////////////*/

#///////////////////////////////////////////////////////////////////////////////
#//!
#//! \file Makefile
#//! \brief A makefile to generate code for the single-zone project.
#//!
#///////////////////////////////////////////////////////////////////////////////

ifndef NUCNET_TARGET
NUCNET_TARGET = ../nucnet-tools-code
endif

ifndef WN_USER_TARGET
WN_USER_TARGET = ../wn_user
endif

ifndef YEN_TARGET
YEN_TARGET = ${WN_USER_TARGET}/build/vendor
endif

#///////////////////////////////////////////////////////////////////////////////
# Here are lines to be edited, if desired.
#///////////////////////////////////////////////////////////////////////////////

SVNURL = http://svn.code.sf.net/p/nucnet-tools/code/trunk
YENURL = https://github.com/mbradle/yen.git

BUILD_DIR = $(WN_USER_TARGET)/build

VENDORDIR = $(BUILD_DIR)/vendor
OBJDIR = $(BUILD_DIR)/obj

NNT_DIR = $(NUCNET_TARGET)/nnt
USER_DIR = $(NUCNET_TARGET)/user

H5 = h5c++

FC = gfortran

#///////////////////////////////////////////////////////////////////////////////
# End of lines to be edited.
#///////////////////////////////////////////////////////////////////////////////

#===============================================================================
# Svn.
#===============================================================================

SVN_CHECKOUT := $(shell if [ ! -d $(NUCNET_TARGET) ]; then svn co $(SVNURL) $(NUCNET_TARGET); else svn update $(NUCNET_TARGET); fi )

#===============================================================================
# Git.
#===============================================================================

GIT_CLONE := $(shell if [ ! -d $(YEN_TARGET)/yen ]; then git clone $(YENURL) $(YEN_TARGET)/yen; else (cd $(YEN_TARGET)/yen && git pull); fi )

#===============================================================================
# Includes.
#===============================================================================

include $(BUILD_DIR)/Makefile
include $(BUILD_DIR)/Makefile.sparse
include $(USER_DIR)/Makefile.inc

VPATH = $(BUILD_DIR):$(NNT_DIR):$(USER_DIR)

H5 = h5c++

#===============================================================================
# Add local directory to includes.
#===============================================================================

CFLAGS += -I ./ -I $(WN_USER_TARGET) -I $(YEN_TARGET)

#===============================================================================
# Debugging, if desired.
#===============================================================================

ifdef DEBUG
  CFLAGS += -DDEBUG
  FFLAGS += -DDEBUG
endif

#===============================================================================
# wn_user.
#===============================================================================

ifdef WN_USER
  CFLAGS += -DWN_USER
endif

ifdef HYDRO_EVOLVE
  CFLAGS += -DHYDRO_EVOLVE
endif

#===============================================================================
# Objects.
#===============================================================================

DAG_OBJ = $(WN_OBJ)          \
          $(WN_USER_OBJ)     \
          $(NNT_OBJ)         \
          $(USER_OBJ)        \
          $(OPTIONS_OBJ)     \

DAG_DEP = $(DAG_OBJ)

CLIBS += -lgfortran

#===============================================================================
# Executables.
#===============================================================================

DAG_EXEC = compute_path_probs evolve_paths

$(DAG_EXEC): $(DAG_DEP)
	$(HC) -Wno-unused-local-typedefs -c -o $(OBJDIR)/$@.o $@.cpp
	$(HC) $(DAG_OBJ) -o $(BINDIR)/$@ $(OBJDIR)/$@.o $(FLIBS) $(CLIBS)

.PHONY all_dag_network: $(DAG_EXEC)

#===============================================================================
# Clean up. 
#===============================================================================

.PHONY: clean_dag_network cleanall_dag_network

clean_dag_network:
	rm -f $(DAG_OBJ)

cleanall_dag_network: clean_dag_network
	rm -f $(BINDIR)/$(DAG_EXEC) $(BINDIR)/$(DAG_EXEC).exe

clean_nucnet:
	rm -fr $(NUCNET_TARGET)
