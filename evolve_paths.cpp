
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for computing network flows.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/format.hpp>
#include "nnt/auxiliary.h"

#ifdef WN_USER
#include "master2.h"
#else
#include "default/master2.h"
#endif

#define    S_TAU_BEGIN      "tau_begin"
#define    S_TAU_END        "tau_end"
#define    S_TAU_NUMBER     "tau_number"

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  my_input.addPositionalParameter<double>(
    S_TAU_BEGIN, 1, "beginning tau (enter as option or positional parameter)",
    "0"
  );

  my_input.addPositionalParameter<double>(
    S_TAU_END, 1, "ending tau (enter as option or positional parameter)",
    "1.e9"
  );

  my_input.addPositionalParameter<size_t>(
    S_TAU_NUMBER, 1, "number tau pts (enter as option or positional parameter)",
    "1000"
  );

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the data.
  //============================================================================

  wn_user::libnucnet_data my_libnucnet_data( v_map );

  //============================================================================
  // Get the zones to study.  Sort them first.
  //============================================================================

  Libnucnet__setZoneCompareFunction(
    my_libnucnet_data.getNucnet(),
    (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
  );

  std::vector<nnt::Zone> zones = my_libnucnet_data.getVectorOfZones();

  //============================================================================
  // Set up graph and evolvers.
  //============================================================================

  wn_user::network_grapher my_network_grapher( v_map );

  auto evolvers = my_network_grapher( zones );

  //============================================================================
  // Evolve.
  //============================================================================

  double d_tau_begin = 
    my_input.getPositionalParameter<double>( v_map, S_TAU_BEGIN );

  double d_tau_end = 
    my_input.getPositionalParameter<double>( v_map, S_TAU_END );

  size_t n =
    my_input.getPositionalParameter<size_t>( v_map, S_TAU_NUMBER );

  size_t m = std::max<size_t>( 1, n - 1 );

  for( size_t i = 0; i < n; i++ )
  {
    double tau = d_tau_begin + (d_tau_end - d_tau_begin) * i / m;
    std::cout << tau;
    BOOST_FOREACH( auto& evolver, evolvers )
    {
      std::cout << " " << evolver( tau );
    }
    std::cout << std::endl;
  }
  
  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
