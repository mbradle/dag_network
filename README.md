# dag_network #

This project computes and evolves paths in linear, acyclic reaction networks.

### Steps to install ###

First, clone the repository by typing 

**git clone https://bitbucket.org/mbradle/dag_network.git**

Next, ensure that wn_user is installed.  If you have not alreadly done so, type

**git clone https://bitbucket.org/mbradle/wn_user.git**

Change into the *dag_network* directory and create the project by typing

**cd dag_network**

**./project_make**

## Steps to compute paths. ##

First, retrieve an example xml file by typing

**curl -o example.xml -J -L  https://osf.io/nfb7u/download**

Next, run the *compute_path_probs* code on an appropriate network xml file.  As an initial example, use the example input file that you retrieved over the web.  Thus, type

**./compute_path_probs --network_xml example.xml --t9 0.5 --rho 1000. --nuc_xpath "[(a = 1) or (z >= 26 and z <= 42 and a - z >= 26 and a - z <= 60)]" --start_vertex fe56 --end_vertex zr97 --init_mass_frac "{n; 1.66e-15}" --number_paths 10 --output_xml out.xml --reac_xpath "[(reactant = 'n' and product = 'gamma') or (product = 'electron')]"**

This example execution will read the data *example.xml* file and
compute the first 10 paths and their probabilities (in order) from the *start_vertex* to the *end_vertex*.  The output will be left in *out.xml*.

Use other options, as desired.  To see the other options available in running the *compute_path_probs* code, type

**./compute_path_probs --help**

and

**./compute_path_probs --prog all**

## Steps to evolve paths. ##

First, be sure you have created an output paths XML file as described above.  In what follows, this
file will be taken to be *out.xml*.

Evolve the paths over an interval.  The interval is generically denoted
*tau*, but in this case it is time in seconds.  Evolve the paths over the
time interval from *tau_begin = 0* to *tau_end = 2.e7*.  Output 1000 points
with *tau_number = 1000*:

**./evolve_paths --libnucnet_xml out.xml --tau_begin 0 --tau_end 2.e7 --tau_number 1000 > out**

The output file *out* will be a text file with each line giving the *tau* (time) and then, for each subsequent column, the evolution for the first, second, third, ... paths.

The *tau* range parameters can also be input as positional parameters.  Thus,
the *./evolve_paths* command above could be executed as

**./evolve_paths --libnucnet_xml out.xml 0 2.e7 1000 > out**

Use other options, as desired.  To see the other options available in running the *evolve_paths* code, type

**./evolve_paths --help**

and

**./evolve_paths --prog all**


