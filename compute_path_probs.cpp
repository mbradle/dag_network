////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for computing network flows.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/format.hpp>

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the data.
  //============================================================================

  wn_user::network_data my_network_data( v_map );

  wn_user::zone_data my_zone_data( v_map, my_network_data.getNucnet() );

  //============================================================================
  // Create and set outputter.
  //============================================================================

  wn_user::outputter my_output( v_map );

  my_output.set( my_network_data.getNucnet() );

  //============================================================================
  // Create work zone.
  //============================================================================

  nnt::Zone zone = my_zone_data.createZone();

  //============================================================================
  // Graph.
  //============================================================================

  wn_user::network_grapher my_network_grapher( v_map );

  std::vector<nnt::Zone> zones = my_network_grapher( zone );

  my_output( zones );

  my_output.write();

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
