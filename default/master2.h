////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017-2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file master.hpp
//! \brief A file to include all helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "libnucnet_data/detail/default.hpp"
#include "libnucnet_data/libnucnet_data.hpp"

#include "nse_corrector/detail/default.hpp"
#include "nse_corrector/nse_corrector.hpp"

#include "screener/detail/default.hpp"
#include "screener/screener.hpp"

#include "rate_modifier/detail/default.hpp"
#include "rate_modifier/rate_modifier.hpp"

#include "rate_registerer/detail/default.hpp"
#include "rate_registerer/rate_registerer.hpp"

#include "rate_computer/default.hpp"

#include "flow_computer/detail/default.hpp"
#include "flow_computer/flow_computer.hpp"

#include "network_grapher/detail/dag_paths_evolve.hpp"
#include "network_grapher/network_grapher.hpp"

#include "inputter/inputter.hpp"
